# -*- coding: UTF-8 -*-
'''
Updated on 2015-01-26
Python: 3.4
@author: Johnny Tsheke -- UQAM
Calcul de la valeur future. Pas encore de validation
'''
P=input("Entrez le capital svp\n")
P=eval(P) #pour avoir la valeur numerique
i=input("Entrez le pourcentage du taux d'intérêt svp\n")
i=eval(i)
i2=i/100.0 #variable temporaire pour ne pas perdre la valeur encodée
I=P*i2
F=P+I
print("Sur une seule période, on a: ")
print("Capital= ",P)
print("Taux d'intérêt= ",i,"% ")
print("Intérêt de l'investissement= ",I)
print("Valeur future de l'investissement= ",F)
